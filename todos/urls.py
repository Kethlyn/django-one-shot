from django.urls import path
from todos.views import (
    todo_list,
    show_details,
    create_view,
    update_view,
    delete_view,
    create_item_view,
    update_item_view,
)

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", show_details, name="todo_list_detail"),
    path("create/", create_view, name="todo_list_create"),
    path("<int:id>/edit/", update_view, name="todo_list_update"),
    path("<int:id>/delete/", delete_view, name="todo_list_delete"),
    path("items/create/", create_item_view, name="todo_item_create"),
    path("items/<int:id>/edit/", update_item_view, name="todo_item_update"),

]
